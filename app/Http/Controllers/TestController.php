<?php

namespace App\Http\Controllers;

use Illuminate\Log\LogManager;

class TestController
{
    /**
     * @var LogManager
     */
    private $logger;

    /**
     * TestController constructor.
     * @param LogManager $logger
     */
    public function __construct(LogManager $logger)
    {
        $this->logger = $logger;
    }

    public function __invoke()
    {
        // channel are configured on config/logging.php
        $this->logger->channel('slack')->info($message = '', $context = []);
        $this->logger->channel('logentries')->emergency($message, $context);
        $this->logger->stack(['logentries', 'slack', 'daily'])->warning($message, $context);

        $this->logger->alert($message, $context); // this is will use default channel from config file
    }
}
